﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace UchotTovarov
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            try
            {
                var user = (from u in db.Users where u.Login_S == textBox1.Text select u).ToArray();
                if (textBox1.Text.Length == 0 || textBox2.Text.Length == 0)
                {
                    MessageBox.Show("Введите логин и пароль");
                }
                else
                {
                    if (textBox1.Text == user[0].Login_S)
                    {
                        if (textBox2.Text == user[0].Password_S)
                        {
                            Hide();
                            FormMenu fm = new FormMenu();
                            fm.Show();
                        }
                        else
                        {
                            MessageBox.Show("Неверный пароль");
                        }
                    }
                }
            }
            catch (SystemException)
            {
                MessageBox.Show("Такого пользователя не существует");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["AboutBox1"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                AboutBox1 ab = new AboutBox1();
                ab.Show();
            }

        }
    }
}
