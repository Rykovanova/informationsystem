﻿namespace UchotTovarov
{
    partial class FormTovNaSkl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTovNaSkl));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tovarynaskladeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.uTDataSet = new UchotTovarov.UTDataSet();
            this.tovary_na_skladeTableAdapter = new UchotTovarov.UTDataSetTableAdapters.Tovary_na_skladeTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.warehouseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.warehouseTableAdapter = new UchotTovarov.UTDataSetTableAdapters.WarehouseTableAdapter();
            this.tovaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tovaryTableAdapter = new UchotTovarov.UTDataSetTableAdapters.TovaryTableAdapter();
            this.iDwDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.iDtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tovarynaskladeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.warehouseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tovaryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDwDataGridViewTextBoxColumn,
            this.iDtDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.tovarynaskladeBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(97, 1);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.Size = new System.Drawing.Size(271, 450);
            this.dataGridView1.TabIndex = 0;
            // 
            // tovarynaskladeBindingSource
            // 
            this.tovarynaskladeBindingSource.DataMember = "Tovary_na_sklade";
            this.tovarynaskladeBindingSource.DataSource = this.uTDataSet;
            // 
            // uTDataSet
            // 
            this.uTDataSet.DataSetName = "UTDataSet";
            this.uTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tovary_na_skladeTableAdapter
            // 
            this.tovary_na_skladeTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 53);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 111);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Удалить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(13, 183);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Печать";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(13, 249);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "Назад";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // warehouseBindingSource
            // 
            this.warehouseBindingSource.DataMember = "Warehouse";
            this.warehouseBindingSource.DataSource = this.uTDataSet;
            // 
            // warehouseTableAdapter
            // 
            this.warehouseTableAdapter.ClearBeforeFill = true;
            // 
            // tovaryBindingSource
            // 
            this.tovaryBindingSource.DataMember = "Tovary";
            this.tovaryBindingSource.DataSource = this.uTDataSet;
            // 
            // tovaryTableAdapter
            // 
            this.tovaryTableAdapter.ClearBeforeFill = true;
            // 
            // iDwDataGridViewTextBoxColumn
            // 
            this.iDwDataGridViewTextBoxColumn.DataPropertyName = "ID_w";
            this.iDwDataGridViewTextBoxColumn.DataSource = this.warehouseBindingSource;
            this.iDwDataGridViewTextBoxColumn.DisplayMember = "Name_w";
            this.iDwDataGridViewTextBoxColumn.HeaderText = "ID_w";
            this.iDwDataGridViewTextBoxColumn.Name = "iDwDataGridViewTextBoxColumn";
            this.iDwDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.iDwDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.iDwDataGridViewTextBoxColumn.ValueMember = "ID_w";
            // 
            // iDtDataGridViewTextBoxColumn
            // 
            this.iDtDataGridViewTextBoxColumn.DataPropertyName = "ID_t";
            this.iDtDataGridViewTextBoxColumn.DataSource = this.tovaryBindingSource;
            this.iDtDataGridViewTextBoxColumn.DisplayMember = "Name_t";
            this.iDtDataGridViewTextBoxColumn.HeaderText = "ID_t";
            this.iDtDataGridViewTextBoxColumn.Name = "iDtDataGridViewTextBoxColumn";
            this.iDtDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.iDtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.iDtDataGridViewTextBoxColumn.ValueMember = "ID_t";
            // 
            // FormTovNaSkl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 450);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormTovNaSkl";
            this.Text = "Таблица Товары на складе";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTovNaSkl_FormClosing);
            this.Load += new System.EventHandler(this.FormTovNaSkl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tovarynaskladeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.warehouseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tovaryBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private UTDataSet uTDataSet;
        private System.Windows.Forms.BindingSource tovarynaskladeBindingSource;
        private UTDataSetTableAdapters.Tovary_na_skladeTableAdapter tovary_na_skladeTableAdapter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.BindingSource warehouseBindingSource;
        private UTDataSetTableAdapters.WarehouseTableAdapter warehouseTableAdapter;
        private System.Windows.Forms.BindingSource tovaryBindingSource;
        private UTDataSetTableAdapters.TovaryTableAdapter tovaryTableAdapter;
        private System.Windows.Forms.DataGridViewComboBoxColumn iDwDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn iDtDataGridViewTextBoxColumn;
    }
}