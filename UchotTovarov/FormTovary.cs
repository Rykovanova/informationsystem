﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PagedList;

namespace UchotTovarov
{
    public partial class FormTovary : Form
    {
        public FormTovary()
        {
            InitializeComponent();
        }
        //Вспомогательные переменные
        int currentPage = 1;
        int pageSize = 40;
        //Создание примитивной графики - линии, фигуры
        Bitmap bmp;
        private void FormTovary_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "uTDataSet.Factories". При необходимости она может быть перемещена или удалена.
            this.factoriesTableAdapter.Fill(this.uTDataSet.Factories);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "uTDataSet.Tovary". При необходимости она может быть перемещена или удалена.
            this.tovaryTableAdapter.Fill(this.uTDataSet.Tovary);

            //Загрузка данных в PagedList
            using (UTDataSet db = new UTDataSet())
            {
                IPagedList<UTDataSet.TovaryRow> list = db.Tovary.OrderBy(p =>
                p.ID_t).ToPagedList(currentPage, pageSize);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["FormMenu"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormMenu fm = new FormMenu();
                fm.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CurrencyManager curman = (CurrencyManager)dataGridView1.BindingContext[dataGridView1.DataSource];
            if (curman.Count > 0)
            {
                curman.RemoveAt(curman.Position);
                tovaryTableAdapter.Update(uTDataSet);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.DataSource = uTDataSet.Tovary;
                tovaryTableAdapter.Update(uTDataSet);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "При сохранении возникла ошибка");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Отрисовка прямоугольника с шириной, равной ширине DataGridView, и высотой, равной количеству строк
            int height = dataGridView1.Height;
            dataGridView1.Height = dataGridView1.RowCount * dataGridView1.RowTemplate.Height;
            bmp = new Bitmap(dataGridView1.Width, dataGridView1.Height);
            dataGridView1.DrawToBitmap(bmp, new Rectangle(0, 0, dataGridView1.Width + 5, dataGridView1.Height));
            dataGridView1.Height = height;
            //вызов Диспетчера печати
            printPreviewDialog1.ShowDialog();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(bmp, 0, 0);
        }

        private void FormTovary_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form fc = Application.OpenForms["FormMenu"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormMenu fm = new FormMenu();
                fm.Show();
            }
        }
    }
}
