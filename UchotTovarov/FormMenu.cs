﻿using System;
using System.Windows.Forms;

namespace UchotTovarov
{
    public partial class FormMenu : Form
    {
        public FormMenu()
        {
            InitializeComponent();
        }
        
        private void button8_Click(object sender, EventArgs e)
        {
            //скрывает форму меню
            this.Hide();

            //проверка коллекции открытых форм 
            Form fc = Application.OpenForms["Form1"];

            if (fc != null)
            {
                //если форма открыта, нужно ее показать
                fc.Show();
            }
            else
            {
                //если нет, создать новый экземпляр и показать его
                Form1 f1 = new Form1();
                f1.Show();
            }
                
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["FormFactories"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormFactories ff = new FormFactories();
                ff.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["FormDistr"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormDistr fd = new FormDistr();
                fd.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["FormTovary"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormTovary ftov = new FormTovary();
                ftov.Show();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["FormPostup"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormPostup fpos = new FormPostup();
                fpos.Show();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["FormSklady"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormSklady fskl = new FormSklady();
                fskl.Show();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["FormTovNaSkl"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormTovNaSkl ftnskl = new FormTovNaSkl();
                ftnskl.Show();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["FormProdaji"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormProdaji fprod = new FormProdaji();
                fprod.Show();
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["FormUsers"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormUsers fus = new FormUsers();
                fus.Show();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["Form2"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                Form2 f2 = new Form2();
                f2.Show();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["Form3"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                Form3 f3 = new Form3();
                f3.Show();
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["Form4"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                Form4 f4 = new Form4();
                f4.Show();
            }
        }
    }
}
