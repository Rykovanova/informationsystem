﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PagedList;

namespace UchotTovarov
{
    public partial class FormDistr : Form
    {
        public FormDistr()
        {
            InitializeComponent();
        }
        //Вспомогательные переменные
        int currentPage = 1;
        int pageSize = 40;
        //Создание примитивной графики - линии, фигуры
        Bitmap bmp;
        private void FormDistr_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "uTDataSet.Distributors". При необходимости она может быть перемещена или удалена.
            this.distributorsTableAdapter.Fill(this.uTDataSet.Distributors);

            using (UTDataSet db = new UTDataSet())
            {
                IPagedList<UTDataSet.DistributorsRow> list = db.Distributors.OrderBy(p =>
                p.ID_d).ToPagedList(currentPage, pageSize);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form fc = Application.OpenForms["FormMenu"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormMenu fm = new FormMenu();
                fm.Show();
            }
        }
        //кнопка удалить
        private void button3_Click(object sender, EventArgs e)
        {
            CurrencyManager curman = (CurrencyManager)dataGridView1.
                BindingContext[dataGridView1.DataSource];
            if (curman.Count > 0)
            {
                curman.RemoveAt(curman.Position);
                distributorsTableAdapter.Update(uTDataSet);
            }
        }
        //кнопка сохранить
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.DataSource = uTDataSet.Distributors;
                distributorsTableAdapter.Update(uTDataSet);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "При сохранении возникла ошибка");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Отрисовка прямоугольника с шириной, равной ширине DataGridView, и высотой, равной количеству строк
            int height = dataGridView1.Height;
            dataGridView1.Height = dataGridView1.RowCount * dataGridView1.RowTemplate.Height;
            bmp = new Bitmap(dataGridView1.Width, dataGridView1.Height);
            dataGridView1.DrawToBitmap(bmp, new Rectangle(0, 0, dataGridView1.Width, dataGridView1.Height));
            dataGridView1.Height = height;
            //вызов Диспетчера печати
            printPreviewDialog1.ShowDialog();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(bmp, 0, 0);
        }

        private void FormDistr_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form fc = Application.OpenForms["FormMenu"];

            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                FormMenu fm = new FormMenu();
                fm.Show();
            }
        }
    }
}
