﻿namespace UchotTovarov
{
    partial class FormDistr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDistr));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.uTDataSet = new UchotTovarov.UTDataSet();
            this.distributorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.distributorsTableAdapter = new UchotTovarov.UTDataSetTableAdapters.DistributorsTableAdapter();
            this.iDdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phonedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.websitedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accountdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.distributorsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDdDataGridViewTextBoxColumn,
            this.namedDataGridViewTextBoxColumn,
            this.addressdDataGridViewTextBoxColumn,
            this.phonedDataGridViewTextBoxColumn,
            this.websitedDataGridViewTextBoxColumn,
            this.accountdDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.distributorsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(142, -1);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.Size = new System.Drawing.Size(656, 448);
            this.dataGridView1.TabIndex = 0;
            // 
            // uTDataSet
            // 
            this.uTDataSet.DataSetName = "UTDataSet";
            this.uTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // distributorsBindingSource
            // 
            this.distributorsBindingSource.DataMember = "Distributors";
            this.distributorsBindingSource.DataSource = this.uTDataSet;
            // 
            // distributorsTableAdapter
            // 
            this.distributorsTableAdapter.ClearBeforeFill = true;
            // 
            // iDdDataGridViewTextBoxColumn
            // 
            this.iDdDataGridViewTextBoxColumn.DataPropertyName = "ID_d";
            this.iDdDataGridViewTextBoxColumn.HeaderText = "ID_d";
            this.iDdDataGridViewTextBoxColumn.Name = "iDdDataGridViewTextBoxColumn";
            this.iDdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // namedDataGridViewTextBoxColumn
            // 
            this.namedDataGridViewTextBoxColumn.DataPropertyName = "Name_d";
            this.namedDataGridViewTextBoxColumn.HeaderText = "Name_d";
            this.namedDataGridViewTextBoxColumn.Name = "namedDataGridViewTextBoxColumn";
            // 
            // addressdDataGridViewTextBoxColumn
            // 
            this.addressdDataGridViewTextBoxColumn.DataPropertyName = "Address_d";
            this.addressdDataGridViewTextBoxColumn.HeaderText = "Address_d";
            this.addressdDataGridViewTextBoxColumn.Name = "addressdDataGridViewTextBoxColumn";
            // 
            // phonedDataGridViewTextBoxColumn
            // 
            this.phonedDataGridViewTextBoxColumn.DataPropertyName = "Phone_d";
            this.phonedDataGridViewTextBoxColumn.HeaderText = "Phone_d";
            this.phonedDataGridViewTextBoxColumn.Name = "phonedDataGridViewTextBoxColumn";
            // 
            // websitedDataGridViewTextBoxColumn
            // 
            this.websitedDataGridViewTextBoxColumn.DataPropertyName = "Website_d";
            this.websitedDataGridViewTextBoxColumn.HeaderText = "Website_d";
            this.websitedDataGridViewTextBoxColumn.Name = "websitedDataGridViewTextBoxColumn";
            // 
            // accountdDataGridViewTextBoxColumn
            // 
            this.accountdDataGridViewTextBoxColumn.DataPropertyName = "Account_d";
            this.accountdDataGridViewTextBoxColumn.HeaderText = "Account_d";
            this.accountdDataGridViewTextBoxColumn.Name = "accountdDataGridViewTextBoxColumn";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(45, 67);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Печать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(45, 150);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Сохранить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(45, 241);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Удалить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(45, 323);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "Назад";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // FormDistr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormDistr";
            this.Text = "Поставщики";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormDistr_FormClosing);
            this.Load += new System.EventHandler(this.FormDistr_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.distributorsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private UTDataSet uTDataSet;
        private System.Windows.Forms.BindingSource distributorsBindingSource;
        private UTDataSetTableAdapters.DistributorsTableAdapter distributorsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn phonedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn websitedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountdDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
    }
}