﻿namespace UchotTovarov
{
    partial class FormPostup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPostup));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.incomingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.uTDataSet = new UchotTovarov.UTDataSet();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.incomingTableAdapter = new UchotTovarov.UTDataSetTableAdapters.IncomingTableAdapter();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.distributorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.distributorsTableAdapter = new UchotTovarov.UTDataSetTableAdapters.DistributorsTableAdapter();
            this.tovaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tovaryTableAdapter = new UchotTovarov.UTDataSetTableAdapters.TovaryTableAdapter();
            this.usersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.usersTableAdapter = new UchotTovarov.UTDataSetTableAdapters.UsersTableAdapter();
            this.iDinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.numberinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.iDuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.priceinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incomingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.distributorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tovaryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDinDataGridViewTextBoxColumn,
            this.dateinDataGridViewTextBoxColumn,
            this.iDdDataGridViewTextBoxColumn,
            this.numberinDataGridViewTextBoxColumn,
            this.iDtDataGridViewTextBoxColumn,
            this.iDuDataGridViewTextBoxColumn,
            this.priceinDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.incomingBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(27, 1);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.Size = new System.Drawing.Size(761, 365);
            this.dataGridView1.TabIndex = 0;
            // 
            // incomingBindingSource
            // 
            this.incomingBindingSource.DataMember = "Incoming";
            this.incomingBindingSource.DataSource = this.uTDataSet;
            // 
            // uTDataSet
            // 
            this.uTDataSet.DataSetName = "UTDataSet";
            this.uTDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(620, 397);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Назад";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(443, 397);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Печать";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(266, 397);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Удалить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(89, 397);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "Сохранить";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // incomingTableAdapter
            // 
            this.incomingTableAdapter.ClearBeforeFill = true;
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // distributorsBindingSource
            // 
            this.distributorsBindingSource.DataMember = "Distributors";
            this.distributorsBindingSource.DataSource = this.uTDataSet;
            // 
            // distributorsTableAdapter
            // 
            this.distributorsTableAdapter.ClearBeforeFill = true;
            // 
            // tovaryBindingSource
            // 
            this.tovaryBindingSource.DataMember = "Tovary";
            this.tovaryBindingSource.DataSource = this.uTDataSet;
            // 
            // tovaryTableAdapter
            // 
            this.tovaryTableAdapter.ClearBeforeFill = true;
            // 
            // usersBindingSource
            // 
            this.usersBindingSource.DataMember = "Users";
            this.usersBindingSource.DataSource = this.uTDataSet;
            // 
            // usersTableAdapter
            // 
            this.usersTableAdapter.ClearBeforeFill = true;
            // 
            // iDinDataGridViewTextBoxColumn
            // 
            this.iDinDataGridViewTextBoxColumn.DataPropertyName = "ID_in";
            this.iDinDataGridViewTextBoxColumn.HeaderText = "ID_in";
            this.iDinDataGridViewTextBoxColumn.Name = "iDinDataGridViewTextBoxColumn";
            this.iDinDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateinDataGridViewTextBoxColumn
            // 
            this.dateinDataGridViewTextBoxColumn.DataPropertyName = "Date_in";
            this.dateinDataGridViewTextBoxColumn.HeaderText = "Date_in";
            this.dateinDataGridViewTextBoxColumn.Name = "dateinDataGridViewTextBoxColumn";
            // 
            // iDdDataGridViewTextBoxColumn
            // 
            this.iDdDataGridViewTextBoxColumn.DataPropertyName = "ID_d";
            this.iDdDataGridViewTextBoxColumn.DataSource = this.distributorsBindingSource;
            this.iDdDataGridViewTextBoxColumn.DisplayMember = "Name_d";
            this.iDdDataGridViewTextBoxColumn.HeaderText = "ID_d";
            this.iDdDataGridViewTextBoxColumn.Name = "iDdDataGridViewTextBoxColumn";
            this.iDdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.iDdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.iDdDataGridViewTextBoxColumn.ValueMember = "ID_d";
            // 
            // numberinDataGridViewTextBoxColumn
            // 
            this.numberinDataGridViewTextBoxColumn.DataPropertyName = "Number_in";
            this.numberinDataGridViewTextBoxColumn.HeaderText = "Number_in";
            this.numberinDataGridViewTextBoxColumn.Name = "numberinDataGridViewTextBoxColumn";
            // 
            // iDtDataGridViewTextBoxColumn
            // 
            this.iDtDataGridViewTextBoxColumn.DataPropertyName = "ID_t";
            this.iDtDataGridViewTextBoxColumn.DataSource = this.tovaryBindingSource;
            this.iDtDataGridViewTextBoxColumn.DisplayMember = "Name_t";
            this.iDtDataGridViewTextBoxColumn.HeaderText = "ID_t";
            this.iDtDataGridViewTextBoxColumn.Name = "iDtDataGridViewTextBoxColumn";
            this.iDtDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.iDtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.iDtDataGridViewTextBoxColumn.ValueMember = "ID_t";
            // 
            // iDuDataGridViewTextBoxColumn
            // 
            this.iDuDataGridViewTextBoxColumn.DataPropertyName = "ID_u";
            this.iDuDataGridViewTextBoxColumn.DataSource = this.usersBindingSource;
            this.iDuDataGridViewTextBoxColumn.DisplayMember = "Full_name";
            this.iDuDataGridViewTextBoxColumn.HeaderText = "ID_u";
            this.iDuDataGridViewTextBoxColumn.Name = "iDuDataGridViewTextBoxColumn";
            this.iDuDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.iDuDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.iDuDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // priceinDataGridViewTextBoxColumn
            // 
            this.priceinDataGridViewTextBoxColumn.DataPropertyName = "Price_in";
            this.priceinDataGridViewTextBoxColumn.HeaderText = "Price_in";
            this.priceinDataGridViewTextBoxColumn.Name = "priceinDataGridViewTextBoxColumn";
            // 
            // FormPostup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormPostup";
            this.Text = "Таблица Поступление товаров";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPostup_FormClosing);
            this.Load += new System.EventHandler(this.FormPostup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incomingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.distributorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tovaryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private UTDataSet uTDataSet;
        private System.Windows.Forms.BindingSource incomingBindingSource;
        private UTDataSetTableAdapters.IncomingTableAdapter incomingTableAdapter;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.BindingSource distributorsBindingSource;
        private UTDataSetTableAdapters.DistributorsTableAdapter distributorsTableAdapter;
        private System.Windows.Forms.BindingSource tovaryBindingSource;
        private UTDataSetTableAdapters.TovaryTableAdapter tovaryTableAdapter;
        private System.Windows.Forms.BindingSource usersBindingSource;
        private UTDataSetTableAdapters.UsersTableAdapter usersTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn iDdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn iDtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn iDuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceinDataGridViewTextBoxColumn;
    }
}